package com.example.schedulerdemo.model;

public class UserPayload {
	private Message message;
	private UserTarget userTarget;
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	public UserTarget getUserTarget() {
		return userTarget;
	}
	public void setUserTarget(UserTarget userTarget) {
		this.userTarget = userTarget;
	}
	@Override
	public String toString() {
		return "UserPayload [message=" + message + ", userTarget=" + userTarget + "]";
	}
	
	

}

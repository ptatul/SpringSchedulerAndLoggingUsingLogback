# SpringSchedulerAndLoggingUsingLogback

In this project two things are availabale one is scheduler and second is logging using logback.xml file.

1. Scheduler :- 
      This contains different kind of scheduler option in this project there is one annotation required which is 
      @Scheduled(fixedRate = 2000) will allow to scheduling
      whereas @Scheduled can take different different parameter as well, options are as follows.
      1. @Scheduled(fixedRate = 2000)
      2. @Scheduled(fixedDelay = 2000)
      3. @Scheduled(fixedRate = 2000, initialDelay = 5000)
      4. @Scheduled(cron = "0 * * * * ?")
      
      In this project database is also called using JPARepository for fetching all record from database.

2. Logging :- 
      For logging need to do following things as mentioned bellow are
      i) First add following snippet into application.properties file
         logging.level.org.springframework=ERROR
         logging.level.com.mkyong=DEBUG
         logging.file=app.log
         logging.pattern.console=%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n
         logging.pattern.file=%d %p %c{1.} [%t] %m%n
     ii) Secound create logback xml file and provide required configuration specify logger name your basce package of project otherwise
         it will not identified.
          <logger name="com.example.schedulerdemo" level="debug" additivity="false">
             <appender-ref ref="FILE-ROLLING"/>
          </logger>
     

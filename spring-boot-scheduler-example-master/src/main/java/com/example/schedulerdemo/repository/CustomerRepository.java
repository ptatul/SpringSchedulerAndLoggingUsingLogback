package com.example.schedulerdemo.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.schedulerdemo.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

}
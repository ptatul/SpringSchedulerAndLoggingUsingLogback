package com.example.schedulerdemo.job;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.example.schedulerdemo.model.Customer;
import com.example.schedulerdemo.repository.CustomerRepository;

@Service
public class SchedulerJob {
	
	@Autowired
	CustomerRepository customerRepository;
	
	public void test() {
		List<Customer> list = ((JpaRepository<Customer, Long>) customerRepository).findAll();
		System.out.println("Size after hitting database : "+list.size());
        System.out.println("This is a test");
    }

}

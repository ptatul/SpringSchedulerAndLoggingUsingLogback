package com.example.schedulerdemo.model;

import java.util.List;

public class UserTarget {
	
	private List<String> userIds;

	public List<String> getUserIds() {
		return userIds;
	}

	
	public void setUserIds(List<String> userIds) {
		this.userIds = userIds;
	}
	
	@Override
	public String toString() {
		return "UserTarget [userIds=" + userIds + "]";
	}

	

}
